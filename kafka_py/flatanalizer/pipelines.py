# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import json


class FlatanalizerPipeline(object):
    def process_item(self, item, spider):
        response = item['response']
        curr_page = [
            page
            for page in response.pagination.pages
            if page.label == 'self'][0]
        with open(f'data/page_{curr_page.num}.json', 'w') as response_file:
            ads = [ad.json() for ad in response.ads]
            json.dump(ads, response_file)
        return item
