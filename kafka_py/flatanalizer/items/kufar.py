from datetime import datetime
from typing import List, NamedTuple, Optional

import scrapy
from pydantic import BaseModel, Schema


class KufarItem(scrapy.Item):
    response = scrapy.Field()


class Geo(NamedTuple):
    lat: float
    lng: float


class KufarAd(BaseModel):
    ad_id: str
    ad_link: str
    address: Optional[str]
    area: Optional[float]
    bathroom: Optional[str]
    category: Optional[str]
    company_ad: Optional[bool]
    condition: Optional[str]
    coordinates: Optional[Geo]
    currency: Optional[str]
    list_id: Optional[int]
    list_time: datetime
    name: str
    price_byn: int
    price_usd: int
    region: int
    remuneration_type: Optional[int]
    rent_type: Optional[int]
    rooms: Optional[int]
    subject: Optional[str]
    type_: str = Schema(Optional, alias="type")


class Page(BaseModel):
    label: str
    num: int
    token: Optional[str]


class KufarPaginator(BaseModel):
    pages: List[Page]


class KufarResponse(BaseModel):
    ads: List[KufarAd]
    pagination: KufarPaginator
    total: int
