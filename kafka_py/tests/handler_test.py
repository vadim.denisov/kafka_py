from datetime import datetime
from unittest.mock import ANY

import pytest

from lambda_helpers import LambdaResponse
from handler import handle


TODAY = datetime.utcnow()


def make_params(output_bucket):
    prefix = '2021'
    return (
        output_bucket,
        200,
        {
            'pipeline_type': 'ortho',
            'output_bucket': output_bucket,
            'prefix': prefix,
            'metadata_prefix': f'{prefix}/metadata',
            'fail_on_unrepairable_gaps': False,
            'write_score_to_storage': False,

        },
    )


@pytest.mark.parametrize(
    [
        'event',
        'output_bucket',
        'sensor_type',
        'status_code_expected',
        'body_expected',
    ],
    [
        make_params('fa-lambdas'),
        make_params('fa-analize'),
        make_params('fa-build'),
        [{}, None, 400, None]
    ],
)
def test_handle(
    event,
    status_code_expected,
    body_expected,
    mocker,
):
    mocker.patch('planner_step.handler.LambdaFunctions')
    mocker.patch('planner_step.handler.LambdaFunctions.invoke')
    mocker.patch('lambda_helpers.error.ErrorHandler')

    try:
        response = handle(event)
        assert response['statusCode'] == status_code_expected
        assert response['body'] == body_expected
    except LambdaResponse as e:
        assert e.status == status_code_expected
