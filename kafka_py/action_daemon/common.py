import asyncio
import logging
import os
import sys
from typing import Optional


def init_logging(
        level: Optional[str] = None,
        fmt: str = "[%(asctime)s][%(name)s]: %(levelname)s: %(message)s"):
    if level is None:
        level = os.getenv("LOG_LEVEL", "INFO")
    logging.basicConfig(format=fmt, level=level)


def asyncio_run(main):
    if not asyncio.iscoroutine(main):
        raise ValueError("a coroutine was expected, got {!r}".format(main))

    loop = _new_loop()

    asyncio.set_event_loop(loop)
    return loop.run_until_complete(main)


def _new_loop() -> asyncio.AbstractEventLoop:
    if sys.platform == "win32":
        return asyncio.ProactorEventLoop()
    return asyncio.new_event_loop()
