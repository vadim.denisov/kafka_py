import asyncio
import logging
from typing import Dict, Optional

logger = logging.getLogger(__name__)


def _with_params(command, params: Optional[Dict]) -> str:
    params = params or {}
    params_str = " ".join(
        f"-{param} {value}"
        for param, value in params.items())
    return " ".join([command, params_str]).strip()


async def run_cmd(command: str, params: Dict = None):
    command = _with_params(command, params)
    logger.info(f"run_cmd: {command}")

    proc = await asyncio.create_subprocess_shell(
        command,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)

    stdout, stderr = await proc.communicate()

    logger.info(f"[{command!r} exited with {proc.returncode}]")
    if stdout:
        logger.info(f"[stdout]\n{stdout.decode()}")
    if stderr:
        logger.error(f"[stderr]\n{stderr.decode()}")
