import asyncio
import json
import logging
import re
from pathlib import Path
from typing import Dict, List

from aiokafka import AIOKafkaConsumer

from action_daemon.action import run_cmd
from action_daemon.handler import BaseHandler, BaseResourceConfig

logger = logging.getLogger(__name__)


class MSKConfig(BaseResourceConfig):
    type_: str
    topic: str
    region: str
    brokers: List[str]

    class Config:
        extra = "forbid"


class MSKHandler(BaseHandler):
    __slots__ = (
        "pattern", "resource", "command", "timeout",
        "consumer")

    def __init__(self, *, pattern: str, command: str,
                 timeout: int, resource: MSKConfig):
        self.resource = resource
        self.pattern = pattern
        self.command = command
        self.timeout = timeout

        self.consumer = AIOKafkaConsumer(
            self.resource.topic,
            loop=asyncio.get_event_loop(),
            bootstrap_servers=self.resource.brokers,
            value_deserializer=self.json_value_deserializer
        )

    async def __aenter__(self) -> "MSKHandler":
        await self.consumer.start()
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self.consumer.stop()

    @staticmethod
    def json_value_deserializer(data: str) -> Dict:
        return json.loads(data)

    async def handle(self):
        logger.info("msk: wait messages...")

        async for msg in self.consumer:
            await self.handle_one(msg.value)

    async def handle_one(self, message: Dict):
        logger.info("msk: handle_one: %s...", str(message)[:24])

        if re.match(self.pattern, message["prefix"]):
            local_folder = self._make_local_folder(message["prefix"])

            await run_cmd(self.command, params={
                "BucketName": message["bucket"],
                "KeyPrefix": message["prefix"],
                "LocalFolder": local_folder,
            })

    def _make_local_folder(self, prefix: str) -> Path:
        path = re.sub(r"(?P<drive>^[a-zA-Z])", r"\g<drive>:", prefix)
        path = f"{path}.0"

        local_folder = Path(path)
        local_folder.mkdir(parents=True, exist_ok=True)

        return local_folder
