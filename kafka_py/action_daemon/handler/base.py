import abc

from pydantic import BaseModel, Field


class BaseResourceConfig(BaseModel):
    type_: str = Field(..., alias="type")

    class Config:
        extra = "allow"


class BaseHandler(abc.ABC):
    @abc.abstractmethod
    def __init__(self, *, pattern: str, command: str,
                 timeout: int, resource: BaseResourceConfig):
        pass

    @abc.abstractmethod
    async def handle(self):
        pass

    @abc.abstractmethod
    async def __aenter__(self):
        pass

    @abc.abstractmethod
    async def __aexit__(self, exc_type, exc, tb):
        pass
