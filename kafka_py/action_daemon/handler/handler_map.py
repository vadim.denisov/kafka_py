from enum import Enum
from typing import Tuple, Type

from action_daemon.handler.base import BaseHandler, BaseResourceConfig
from action_daemon.handler.msk import MSKConfig, MSKHandler
from action_daemon.handler.sqs import SQSConfig, SQSHandler


class HandlerMap(Enum):
    SQS = (SQSHandler, SQSConfig)
    KAFKA = (MSKHandler, MSKConfig)

    @classmethod
    def get(cls, type_: str) -> Tuple[Type[BaseHandler],
                                      Type[BaseResourceConfig]]:
        handler_classes = getattr(cls, type_.upper(), None)
        if handler_classes is None:
            msg = f"Handler class not found, provider={type_}"
            raise ValueError(msg)
        handler_cls, config_cls = handler_classes.value
        return handler_cls, config_cls
