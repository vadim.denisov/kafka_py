import json
import logging
import re
from typing import Dict, List

import aiobotocore
from cached_property import cached_property

from action_daemon.action import run_cmd
from action_daemon.handler import BaseHandler, BaseResourceConfig
from action_daemon.handler.exceptions import SQSError

logger = logging.getLogger(__name__)


class SQSConfig(BaseResourceConfig):
    type_: str
    name: str
    region: str

    class Config:
        extra = "forbid"


class SQSHandler(BaseHandler):
    __slots__ = (
        "pattern", "resource", "command", "timeout",
        "session",)

    def __init__(self, *, pattern: str, command: str,
                 timeout: int, resource: SQSConfig):
        self.resource: SQSConfig = SQSConfig.parse_obj(resource)
        self.pattern = pattern
        self.command = command
        self.timeout = timeout

        self.session = aiobotocore.get_session()

    @cached_property
    async def queue_url(self) -> str:
        response = await self.sqs.get_queue_url(
            QueueName=self.resource.name)
        return response["QueueUrl"]

    @cached_property
    def sqs(self) -> "aiobotocore.client.SQS":
        return self.session.create_client(
            self.resource.type_,
            region_name=self.resource.region)

    async def __aenter__(self) -> "SQSHandler":
        await self.sqs.__aenter__()
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self.sqs.__aexit__(exc_type, exc, tb)
        del self.__dict__["sqs"]

    async def receive_message(self):
        return await self.sqs.receive_message(
            QueueUrl=await self.queue_url,
            WaitTimeSeconds=self.timeout)

    async def delete_message(self, receipt_handle: str):
        await self.sqs.delete_message(
            QueueUrl=await self.queue_url,
            ReceiptHandle=receipt_handle)

    def _decode_body(self, message: Dict) -> Dict:
        body = message.get("Body")
        if body is None:
            raise SQSError("Body does not exists", message)

        try:
            body = json.loads(body)
        except json.JSONDecodeError as error:
            raise SQSError("Body decode error", body) from error
        except TypeError as error:
            err_msg, *_ = error.args
            raise SQSError(err_msg, body) from error

        return body

    def _decode_keys(self, body: Dict) -> List[str]:
        records = body.get("Records")
        if records is None:
            raise SQSError("Records is empty", body)

        try:
            keys = [record["s3"]["object"]["key"] for record in records]
        except (KeyError, TypeError) as error:
            raise SQSError("Invalid 'Records' structure", records) from error

        return keys

    async def handle(self):
        logger.info("sqs: wait messages...")
        response = await self.receive_message()

        messages = response.get("Messages")
        if not messages:
            logger.warning(SQSError("Queue is empty", response))
            return

        for message in messages:
            await self.handle_one(message)

    async def handle_one(self, message: Dict):
        logger.info("sqs: handle_one: %s...", str(message)[:24])

        receipt_handle = message.get("ReceiptHandle")
        if receipt_handle is None:
            raise SQSError("ReceiptHandle missing", message)

        body = self._decode_body(message)
        keys = self._decode_keys(body)

        if any(re.match(self.pattern, key) for key in keys):
            await run_cmd(self.command)

        await self.delete_message(receipt_handle)
