import os
from typing import Any, List, Optional

import yaml
from pydantic import BaseModel

from action_daemon.handler import BaseHandler, BaseResourceConfig, HandlerMap


class ActionConfig(BaseModel):
    name: str

    pattern: str
    command: str
    timeout: int
    resource: BaseResourceConfig

    handler: Optional[BaseHandler]

    class Config:
        arbitrary_types_allowed = True

    def _create_handler(self) -> BaseHandler:
        handler_cls, config_cls = HandlerMap.get(self.resource.type_)
        resource = config_cls.parse_obj(self.resource)
        return handler_cls(
            pattern=self.pattern,
            command=self.command,
            timeout=self.timeout,
            resource=resource)

    def __init__(self, **data: Any):
        super().__init__(**data)
        self.handler = self._create_handler()


class ApplicationConfig(BaseModel):
    actions: List[ActionConfig]


def read_config(path) -> ApplicationConfig:
    if not os.path.exists(path):
        raise OSError(f"{path} does not exists.")

    with open(path) as config_file:
        config = yaml.safe_load(config_file)

    actions = [ActionConfig(**action) for action in config["actions"]]
    return ApplicationConfig(actions=actions)
