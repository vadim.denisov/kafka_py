import logging
import time
import random
import string

def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))
logging.basicConfig(level='INFO')


if __name__ == "__main__":
    logging.info('SQS: wait messages...')
    logging.info('MSK: wait messages...')
    for i in range(40):
        m = randomString(15)
        if random.random() < 0.5:
            logging.info('SQS: Send message, msg=%s', m)
            logging.info(r'SQS: Run cmd: echo {event}')
        else:
            logging.info('MSK: Send message, msg=%s', randomString(15))
            logging.info(r'MSK: Run cmd: echo {event}')
        logging.info('BASH: echo %s', m)

    time.sleep(120)