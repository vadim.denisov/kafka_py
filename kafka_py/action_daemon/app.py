import argparse
import asyncio
import logging
from pathlib import Path

from botocore.exceptions import BotoCoreError, NoCredentialsError

from action_daemon.common import asyncio_run, init_logging
from action_daemon.config import ApplicationConfig, read_config
from action_daemon.handler import BaseHandler
from action_daemon.handler.exceptions import SQSError

logger = logging.getLogger(__name__)

BASE_DIR = Path(__file__).parent.parent
DEFAULT_CONFIG_PATH = BASE_DIR / "config.yml"


async def create_worker(handler: BaseHandler):
    async with handler:
        while True:
            try:
                await handler.handle()
            except NoCredentialsError as error:
                logger.error("%s", error)
                logger.warning("Close %s", type(handler).__name__)
                break
            except (SQSError, BotoCoreError) as error:
                logger.error("%s", error)


async def run_handlers(config: ApplicationConfig):
    tasks = []
    for action_cfg in config.actions:
        worker = create_worker(handler=action_cfg.handler)
        task = asyncio.ensure_future(worker)
        tasks.append(task)

    done, pending = await asyncio.wait(tasks)
    logger.info("Done: %s", done)
    logger.info("Pending: %s", pending)


def init_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("-l", "--log", default="INFO",
                        help="Log level")
    parser.add_argument("-c", "--config", default=DEFAULT_CONFIG_PATH,
                        help="path to config file")

    return parser.parse_args()


def main():
    args = init_parser()
    init_logging(args.log)

    logger.info("Read config: %s", args.config)
    config = read_config(args.config)

    try:
        asyncio_run(run_handlers(config))
    except KeyboardInterrupt:
        logger.info("Exit.")
