import logging
import time
import random
import string

def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))
logging.basicConfig(level='INFO')


if __name__ == "__main__":
    logging.info('Run producer...')
    for i in range(20):
        logging.info('Send message, msg=%s', randomString(15))
        logging.info('Sleep 120 sec')
    time.sleep(120)